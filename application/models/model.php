<?php 
/**
* 
*/
class Model extends CI_Model
{
	public function kelas()
	{
		$query = $this->db->query("SELECT DISTINCT kelas FROM table_sekdos");
		return $query->result();
	}
	public function search()
	{
		$b = $this->input->post('lbl_ajaran');
		$c = $this->input->post('kelas');
		if ($b && $c) {

			$query = $this->db->query("SELECT DISTINCT table_sekdos.kelas , table_sekdos.kode_mk , table_sekdos.semester , table_psa.nama_mk , table_psa.nama_dosen FROM table_sekdos INNER JOIN table_thajaran ON table_sekdos.semester=table_thajaran.kd_ajaran INNER JOIN table_psa ON table_sekdos.kode_mk=table_psa.kode_mk WHERE table_sekdos.semester LIKE '%$b%' AND table_sekdos.kelas LiKE '%$c%'");
			return $query;
		}
		else
		{
			return false;
		}
	}
	public function data_absensi()
	{
		$kode_mk = $this->input->post("fkode_mk");
		$kelas = $this->input->post("fkelas");
		$semester = $this->input->post("fdsmt");
		$nama_mk = $this->input->post("fmatkul");
		$dosen = $this->input->post("fnama_dosen");
		$arrayName = array('kode_mk' =>$kode_mk,
							'kelas' =>$kelas,
							'semester'=>$semester
						  );
		$query = $this->db->query("SELECT * FROM table_sekdos sds INNER JOIN table_thajaran th on sds.semester=th.kd_ajaran INNER JOIN table_psa psa on sds.kode_mk=psa.kode_mk WHERE sds.kode_mk='$kode_mk' AND sds.kelas='$kelas' AND sds.semester='$semester' AND psa.nama_mk='$nama_mk'");
		// $this->db->select("*");
		// $this->db->from("table_sekdos");
		// $this->db->join("table_thajaran",'table_thajaran.kd_ajaran = table_sekdos.semester');
		// $this->db->join("table_psa",'table_.kd_ajaran = table_sekdos.semester');
		// $this->db->where($arrayName);
		// $query = $this->db->get();
		return $query;
	}
}