<?php 
if ($fabsensi->num_rows() > 0)
{
	$row = $fabsensi->row(); 
	?>
	<form action="<?php echo site_url('absensi/searchdata') ?>" id="formsearch" method="post">
		<div class="form-group">
			<label class="col-md-3 control-label">Tahun Ajaran :</label>
			<div class="col-md-9" style="width:40%;">
				<?php echo $row->semester; ?>
			</div>
		</div>
		<br>
		<br>
		<div class="form-group">
			<label class="col-md-3 control-label">Kelas :</label>
			<div class="col-md-9" style="width:40%;">
				<?php echo $row->kelas; ?>
			</div>
		</div>
		<br>
		<div class="form-group">
			<label class="col-md-3 control-label">Mata Kuliah :</label>
			<div class="col-md-9" style="width:40%;">
				<?php echo $row->nama_mk; ?>
				<?php echo $row->kode_mk; ?>
			</div>
		</div>
		<br>
		<div class="form-group">
			<label class="col-md-3 control-label">Dosen :</label>
			<div class="col-md-9" style="width:40%;">
				Belum Ada Data
			</div>
		</div>
		<?php } ?>
		<br>
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cogs font-green-sharp"></i>
							<span class="caption-subject font-green-sharp bold uppercase">Form Absensi</span>
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
							<a href="#portlet-config" data-toggle="modal" class="config">
							</a>
							<a href="javascript:;" class="reload">
							</a>
							<a href="javascript:;" class="remove">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-6">

								</div>
								<div class="col-md-6">
									<div class="btn-group pull-right">
										<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
										</button>
										<ul class="dropdown-menu pull-right">
											<li>
												<a href="javascript:;">
													Print </a>
												</li>
												<li>
													<a href="javascript:;">
														Save as PDF </a>
													</li>
													<li>
														<a href="javascript:;">
															Export to Excel </a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<table class="table table-striped table-bordered table-hover" id="sample_1">
										<thead>
											<tr>
											<td colspan="30" align="center" style="background-color:#EEE;border:solid 1px #ccc;">&nbsp;Jumlah Hadir selama 14 Minggu</td>
											</tr>

											<tr>

												<th rowspan="3">No.</th>
												<th rowspan="3">Nama Mahasiswa</th>
												<th rowspan="3">NPM</th>

												<td>&nbsp;1</td>
												<td>&nbsp;2</td>
												<td>&nbsp;3</td>
												<td>&nbsp;4</td>
												<td>&nbsp;5</td>
												<td>&nbsp;6</td>
												<td>&nbsp;7</td>
												<td>&nbsp;8</td>
												<td>&nbsp;9</td>
												<td>&nbsp;10</td>
												<td>&nbsp;11</td>
												<td>&nbsp;12</td>
												<td>&nbsp;13</td>
												<td>&nbsp;14</td>

												<th rowspan="3">Jumlah Hadir</th>
												<th rowspan="3">Presentasi</th>
												<th rowspan="3">Keterangan</th>
											</tr>

											<tr>

												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>

												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>

											</tr>

										</thead>
										<?php $no=0; foreach ($fabsensi->result() as $key => $value) { $no++;?>
										<tbody>
											<tr>
												
												<th rowspan="3"><?php echo $no; ?></th>
												<th rowspan="3"><?php echo $value->nama_mhs; ?></th>
												<th rowspan="3"><?php echo $value->npm; ?></th>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>

												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>
												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>

												<td>&nbsp;<input type="checkbox" class="checkboxes" id="ceklishadir" value="1"/></td>

												
												<th rowspan="3">Jumlah Hadir</th>
												<th rowspan="3">Presentasi</th>
												<th rowspan="3">Ket</th>
											</tr>

											<tr>

												
											</tr>
										</tbody>
									<?php } ?>
									</table>
									<button id="Simpan" class="btn green">
										Simpan <i class="fa fa-save"></i>
									</button>
								</div>
							</div>
							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>
					<script src="<?php echo base_url('assets') ?>/global/plugins/jquery.min.js" type="text/javascript"></script>
					<script src="<?php echo base_url('assets') ?>/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
					<script src="<?php echo base_url('assets') ?>/admin/pages/scripts/table-managed.js"></script>
					<script type="text/javascript">
						$(document).ready(function() {
							$("#ceklishadir").change(function() {
								console.log($(this));
							});
						});
					</script>
					<script>
						jQuery(document).ready(function() {       
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
TableManaged.init();
});
					</script>
					<script>
						(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
							(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
							m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
						})(window,document,'script','../../../../../../www.google-analytics.com/analytics.js','ga');
						ga('create', 'UA-37564768-1', 'keenthemes.com');
						ga('send', 'pageview');
					</script>